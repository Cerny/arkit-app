//
//  Logger.swift
//  CrowningArts
//
//  Created by Michal Černý on 02.12.2020.
//  Copyright © 2020 DAMI development s.r.o. All rights reserved.
//

import Foundation
import os

enum Logger {

    enum Category {
        case general
        case detectedPlanes
    }

    private static var bundleId: String {
        Bundle.main.bundleIdentifier!
    }

    #if DEBUG
    static func error(_ log: String, category: Category) {
        if CommandLine.arguments.contains("-allow-os-log") {
            os_log("%{public}@", log: OSLog(subsystem: bundleId, category: category.name), type: .error, log)
        }
    }

    static func normal(_ log: String, category: Category) {
        if CommandLine.arguments.contains("-allow-os-log") {
            os_log("%{public}@", log: OSLog(subsystem: bundleId, category: category.name), type: .default, log)
        }
    }
    #else
    static func error(_ log: String, category: Category) { }
    static func normal(_ log: String, category: Category) { }
    #endif
}

extension Logger.Category {
    var name: String {
        switch self {
        case .general:
            return "General"
        case .detectedPlanes:
            return "Detected planes"
        }
    }
}
