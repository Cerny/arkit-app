//
//  Effect.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit

enum Effect: CaseIterable {
    case mono, tonal, noir, monochrome, sepia, fade, chrome, process, transfer, instant
    
    var name: String {
        switch self {
        case .mono: return "Mono"
        case .tonal: return "Tonal"
        case .noir: return "Noir"
        case .monochrome: return "Monochrome"
        case .sepia: return "Sepia"
        case .fade: return "Fade"
        case .chrome: return "Chrome"
        case .process: return "Process"
        case .transfer: return "Transfer"
        case .instant: return "Instant"
        }
    }
    
    var filterName: String {
        switch self {
        case .mono: return "CIPhotoEffectMono"
        case .tonal: return "CIPhotoEffectTonal"
        case .noir: return "CIPhotoEffectNoir"
        case .monochrome: return "CIColorMonochrome"
        case .sepia: return "CISepiaTone"
        case .fade: return "CIPhotoEffectFade"
        case .chrome: return "CIPhotoEffectChrome"
        case .process: return "CIPhotoEffectProcess"
        case .transfer: return "CIPhotoEffectTransfer"
        case .instant: return "CIPhotoEffectInstant"
        }
    }
    
    var filter: CIFilter? {
        CIFilter(name: filterName)
    }
}

