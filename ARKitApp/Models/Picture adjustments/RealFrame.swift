//
//  RealFrame.swift
//  ARKitApp
//
//  Created by Michal Černý on 16.04.2021.
//

import UIKit

struct RealFrame: Hashable {
    
    enum Constants {
        static let defaultColor: UIColor = .brown
    }
    
    enum Appearance: String, CaseIterable {
        case frame3
        case frame6
        case frame5
        case frame4
        case frame8
        case frame7
        case frame2
        
        static let `default` = Self.frame3
    }
    
    var appearance: Appearance
    var color: UIColor
    
    static let `default` = Self.init(appearance: .default, color: .black)
}

extension RealFrame.Appearance {
    var image: UIImage? {
        return UIImage(named: rawValue)
    }
    
    var sideObjectURL: URL {
        var resource: String {
            switch self {
            case .frame3: return "3S"
            case .frame6: return "6S"
            case .frame5: return "5S"
            case .frame4: return "4S"
            case .frame8: return "8S"
            case .frame7: return "7S"
            case .frame2: return "2S"
            }
        }
        return Bundle.main.url(forResource: resource, withExtension: "usdz")!
    }
    
    var cornerObjectURL: URL {
        var resource: String {
            switch self {
            case .frame3: return "3C"
            case .frame6: return "6C"
            case .frame5: return "5C"
            case .frame4: return "4C"
            case .frame8: return "8C"
            case .frame7: return "7C"
            case .frame2: return "2C"
            }
        }
        return Bundle.main.url(forResource: resource, withExtension: "usdz")!
    }
}
