//
//  PictureColor.swift
//  ARKitApp
//
//  Created by Michal Černý on 18.04.2021.
//

import Foundation

class PictureColor {
    
    init(brightness: Int = 0, contrasts: Int = 0, saturation: Int = 0) {
        self.brightness = brightness
        self.contrast = contrasts
        self.saturation = saturation
    }
    
    /// Range <-100;100>
    var brightness: Int
    /// Range <-100;100>
    var contrast: Int
    /// Range <-100;100>
    var saturation: Int
    
    /// Range <0;1>
    var brightnessPercentNormalized: Double {
        (Double(brightness) + 100) / 200
    }
    /// Range <0;1>
    var contrastPercentNormalized: Double {
        (Double(contrast) + 100) / 200
    }
    /// Range <0;1>
    var saturationPercentNormalized: Double {
        (Double(saturation) + 100) / 200
    }
    
    var ciFilterBrightness: Float {
        let min: Float = -1
        let max: Float = 1
        return min + Float(brightnessPercentNormalized) * (max - min)
    }
    var ciFilterContrast: Float {
        let min: Float = 0
        let max: Float = 2
        return min + Float(contrastPercentNormalized) * (max - min)
    }
    var ciFilterSaturation: Float {
        let min: Float = 0
        let max: Float = 2
        return min + Float(saturationPercentNormalized) * (max - min)
    }
}
