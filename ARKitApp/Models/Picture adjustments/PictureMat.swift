//
//  PictureMat.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit

class PictureMat {
    static let defaultColor: UIColor = .white
    
    var widthInMeters: CGFloat = 0
    var color = defaultColor
    
    init(widthInMeters: CGFloat = 0, color: UIColor = defaultColor) {
        self.widthInMeters = widthInMeters
        self.color = color
    }
}
