//
//  UICollectionView.swift
//  ARKitApp
//
//  Created by Michal Černý on 04.12.2020.
//

import UIKit

fileprivate func classNameOf(aClass: AnyClass) -> String {
    return NSStringFromClass(aClass).components(separatedBy: ".").last!
}

extension UICollectionViewCell {
    public static var defaultReuseIdentifier: String {
        return classNameOf(aClass: self)
    }
    
    public static var nibName: String {
        return classNameOf(aClass: self)
    }
}

extension UICollectionView {
    
    /// Registers given cells types using as nib name `nibName` static property (= class name)
    /// and `defaultReuseIdentifier`
    public func registerCellsWithNibs(_ cellTypes: [UICollectionViewCell.Type]) {
        cellTypes.forEach { cell in
            register(UINib(nibName: cell.nibName, bundle: nil), forCellWithReuseIdentifier: cell.defaultReuseIdentifier)
        }
    }
    
    /// Registers given cells types (**without nib**) with reuse identifier `defaultReuseIdentifier`
    public func registerCellsWithClasses(_ cellTypes: [UICollectionViewCell.Type]) {
        cellTypes.forEach { cell in
            register(cell, forCellWithReuseIdentifier: cell.defaultReuseIdentifier)
        }
    }
    
    /// Dequeuing and forced casting together
    public func dequeueReusableCell<CellType: UICollectionViewCell>(ofClass cellClass: CellType.Type, for indexPath: IndexPath) -> CellType {
        return dequeueReusableCell(withReuseIdentifier: cellClass.defaultReuseIdentifier, for: indexPath) as! CellType
    }
    
}
