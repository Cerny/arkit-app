//
//  UIView.swift
//  ARKitApp
//
//  Created by Michal Černý on 04.12.2020.
//

import UIKit

extension UIView {
    
    func removeConstraints(_ types: [NSLayoutConstraint.Attribute]) {
        let filteredConstraints = constraints.filter { types.contains($0.firstAttribute) }
        filteredConstraints.forEach {
            removeConstraint($0)
        }
    }
    
    func constraints(equalTo view: UIView) -> [NSLayoutConstraint] {
        [
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
    }
    
    func setConstraints(equalTo view: UIView) {
        let viewConstrains = constraints(equalTo: view)
        NSLayoutConstraint.activate(viewConstrains)
    }
    
    func setConstraintsEqualToSuperview() {
        guard let superview = superview else { return }
        setConstraints(equalTo: superview)
    }
    
    static func makeSpacer(space: CGFloat) -> UIView {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: space).isActive = true
        return view
    }
    
    public func addShadow() {
        addShadow(color: UIColor.black.withAlphaComponent(0.4), offset: CGSize(width: 0, height: 8), radius: 16, opacity: 1)
    }

    public func addShadow(color: UIColor, offset: CGSize, radius: CGFloat, opacity: Float) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
    }
}
