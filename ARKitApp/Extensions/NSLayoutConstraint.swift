//
//  NSLayoutConstraint.swift
//  ARKitApp
//
//  Created by Michal Černý on 04.12.2020.
//

import UIKit

extension NSLayoutConstraint {
    
    // credit: https://www.innoq.com/en/blog/ios-auto-layout-problem/
    public class func useAndActivate(_ constraints: [NSLayoutConstraint]) {
        for constraint in constraints {
            if let view = constraint.firstItem as? UIView {
                 view.translatesAutoresizingMaskIntoConstraints = false
            }
        }
        activate(constraints)
    }
    
}
