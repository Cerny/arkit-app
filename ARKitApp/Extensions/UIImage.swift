//
//  UIImage.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit

extension UIImage {
    
    func changeTransparentPixels(to backgroundColor: UIColor) -> UIImage {
        let image = self
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        let imageRect: CGRect = CGRect(x: 0.0, y: 0.0, width: image.size.width, height: image.size.height)
        let ctx: CGContext = UIGraphicsGetCurrentContext()!
        // Draw background
        ctx.setFillColor(backgroundColor.cgColor)
        ctx.fill(imageRect)
        image.draw(in: imageRect, blendMode: .normal, alpha: 1.0)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func process(with effect: Effect) -> UIImage? {
        guard let cgImage = cgImage else { return nil }
        let ciImage = CIImage(cgImage: cgImage)
        let processedImage = ciImage.applying(effect: effect)
        guard let resultCGImage = CIContext(options: nil).createCGImage(processedImage, from: processedImage.extent) else { return nil }
        return UIImage(cgImage: resultCGImage)
    }
    
}

