//
//  CIImage.swift
//  ARKitApp
//
//  Created by Michal Černý on 18.04.2021.
//

import UIKit

extension CIImage {
    
    func applying(brightness: Float, contrast: Float, saturation: Float) -> CIImage {
        applyingFilter(
            "CIColorControls",
            parameters: [
                kCIInputBrightnessKey: NSNumber(value: brightness),
                kCIInputContrastKey: NSNumber(value: contrast),
                kCIInputSaturationKey: NSNumber(value: saturation)
            ])
    }
    
    func applying(effect: Effect?) -> CIImage {
        if let effect = effect {
            return self.applyingFilter(effect.filterName)
        } else {
            return self
        }
    }
    
}
