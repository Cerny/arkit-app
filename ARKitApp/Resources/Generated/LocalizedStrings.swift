// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// Add
  internal static let add = L10n.tr("Localizable", "add")
  /// Are you sure to delete this photo?
  internal static let areYouSureDelete = L10n.tr("Localizable", "areYouSureDelete")
  /// Brightness
  internal static let brightness = L10n.tr("Localizable", "brightness")
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "cancel")
  /// Color
  internal static let color = L10n.tr("Localizable", "color")
  /// Contrast
  internal static let contrast = L10n.tr("Localizable", "contrast")
  /// Delete
  internal static let delete = L10n.tr("Localizable", "delete")
  /// Effects
  internal static let effects = L10n.tr("Localizable", "effects")
  /// Grids
  internal static let grids = L10n.tr("Localizable", "grids")
  /// Mat
  internal static let mat = L10n.tr("Localizable", "mat")
  /// OK
  internal static let ok = L10n.tr("Localizable", "ok")
  /// Frame
  internal static let realFrame = L10n.tr("Localizable", "realFrame")
  /// Reset
  internal static let reset = L10n.tr("Localizable", "reset")
  /// Rotate
  internal static let rotate = L10n.tr("Localizable", "rotate")
  /// Saturation
  internal static let saturation = L10n.tr("Localizable", "saturation")
  /// Shadow
  internal static let shadow = L10n.tr("Localizable", "shadow")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
