//
//  ColorPickerViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit
import Colorful

protocol ColorPickerViewControllerDelegate: class {
    func colorChanged(newColor: UIColor)
}

class ColorPickerViewController: UIViewController {
    
    private weak var colorPickersStackView: UIStackView!
    
    weak var delegate: ColorPickerViewControllerDelegate?
    private weak var colorPicker: ColorPicker!
    private let initialColor: UIColor
    
    init(initialColor: UIColor) {
        self.initialColor = initialColor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(color: initialColor)
    }
    
    func setup(color: UIColor) {
        colorPicker.set(color: color, colorSpace: .sRGB)
    }
    
    @objc private func dismissScreen() {
        dismiss(animated: true)
    }
    
    @objc private  func colorChanged(){
        delegate?.colorChanged(newColor: colorPicker.color)
    }
    
}

// MARK: - UI
extension ColorPickerViewController {
    
    func setupUI() {
        addStackView()
        setupGestureRecognizer()
    }
    
    func addStackView() {
        let colorPicker = makeColorPicker()
        self.colorPicker = colorPicker
        
        let stackView = UIStackView(arrangedSubviews: [colorPicker])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5),
        ])
        self.colorPickersStackView = stackView
    }
    
    private func makeColorPicker() -> ColorPicker {
        let colorPicker = ColorPicker()
        colorPicker.addTarget(self, action: #selector(colorChanged), for: .valueChanged)
        colorPicker.set(color: .blue, colorSpace: .sRGB)
        return colorPicker
    }
    
    private func setupGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissScreen))
        view.addGestureRecognizer(tapGestureRecognizer)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action:  #selector(dismissScreen))
        view.addGestureRecognizer(panGestureRecognizer)
    }
    
}
