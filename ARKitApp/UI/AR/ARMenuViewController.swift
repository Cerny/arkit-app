//
//  ARMenuViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 05.12.2020.
//

import UIKit
import Combine

class ARMenuViewController: UIViewController {
    
    private weak var resetButton: UIButton!
    private weak var addButton: UIButton!
    weak var gridsSwitch: UISwitch!
    
    private var imagePicker: UIImagePickerController?
    
    var pictureToAdd = PassthroughSubject<UIImage, Never>()
    var shouldReset = PassthroughSubject<Void, Never>()
    var areGridsVisible = PassthroughSubject<Bool, Never>()
    private var disposeBag = Set<AnyCancellable>()
    
    override func loadView() {
        super.loadView()
        setupUI()
    }
    
    // MARK: - Actions
    
    @objc private func resetButtonClicked() {
        shouldReset.send(())
    }
    
    @objc private func addButtonClicked() {
        guard !developmentMode else {
            pictureToAdd.send(Asset.uhk.image)
            return
        }
        
        let pickerController = ImagePickerViewController(source: .photoLibrary)
        pickerController.selectedImage
            .sink { [weak self] in
                self?.pictureToAdd.send($0)
            }
            .store(in: &disposeBag)
        present(pickerController, animated: true)
    }
    
    @objc private func gridsSwitchValueChanged(_ sender: UISwitch) {
        areGridsVisible.send(sender.isOn)
    }
    
}

// MARK: - UI
extension ARMenuViewController {
    
    private func setupUI() {
        let resetButton = makeButton(title: L10n.reset, selector: #selector(resetButtonClicked))
        self.resetButton = resetButton
        let addButton = makeButton(title: L10n.add, selector: #selector(addButtonClicked))
        self.addButton = addButton
        
        let gridsSwitch = UISwitch()
        gridsSwitch.addTarget(self, action: #selector(gridsSwitchValueChanged), for: .valueChanged)
        self.gridsSwitch = gridsSwitch
        
        let gridsLabel = UILabel()
        gridsLabel.text = L10n.grids
        gridsLabel.textColor = .white
        gridsLabel.font = .systemFont(ofSize: 14)
        
        let arrangedSubviews: [UIView] = [
            resetButton,
            addButton,
            UIView(), // spacer
            gridsLabel,
            gridsSwitch,
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        NSLayoutConstraint.useAndActivate([
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 4),
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -4),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
        ])
    }
    
    private func makeButton(title: String, selector: Selector) -> UIButton {
        let button = UIButton(type: .system)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
    
}
