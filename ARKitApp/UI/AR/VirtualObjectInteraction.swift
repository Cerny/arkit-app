//
//  VirtualObjectInteraction.swift
//  ARKitApp
//
//  Created by Michal Černý on 01.01.2021.
//

import UIKit
import ARKit

typealias VirtualObject = SCNPicture
typealias VirtualObjectARView = ARSCNView

class VirtualObjectInteraction: NSObject, UIGestureRecognizerDelegate {

    private weak var sceneView: ARSCNView!
    private weak var viewController: ARViewController!
    private var model: ARModel {
        viewController.model
    }
    private var currentTrackingPosition: CGPoint?
    @Published private(set) var selectedObject: VirtualObject?
    private var trackedObject: VirtualObject? {
        didSet {
            guard trackedObject != nil else { return }
            selectedObject = trackedObject
        }
    }
    
    init(sceneView: VirtualObjectARView, viewController: ARViewController) {
        self.sceneView = sceneView
        self.viewController = viewController
        super.init()
    }
    
    func setupGestureRecognizers() {
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(didPinch))
        sceneView.addGestureRecognizer(pinchGestureRecognizer)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan))
        sceneView.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc
    private func didPinch(_ gestureRecognizer: UIPinchGestureRecognizer) {
        guard let selectedPicture = selectedObject else { return }
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            selectedPicture.resize(scale: gestureRecognizer.scale)
            gestureRecognizer.scale = 1
        }
    }
    
    @objc
    private func didTap(_ gestureRecognizer: UITapGestureRecognizer) {
        deselectObject()
        if let touchedPicture = objectInteracting(with: gestureRecognizer, in: sceneView) {
            selectObject(touchedPicture)
        }
    }
    
    @objc
    private func didPan(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            if let object = objectInteracting(with: gesture, in: sceneView) {
                trackedObject = object
            }
        case .changed:
            guard let object = trackedObject else { return }
            translate(object, basedOn: updatedTrackingPosition(for: object, from: gesture))
            gesture.setTranslation(.zero, in: sceneView)
        default:
            // Reset the current position tracking.
            currentTrackingPosition = nil
            trackedObject = nil
        }
    }
    
    private func translate(_ object: VirtualObject, basedOn screenPos: CGPoint) {
        guard let query = sceneView.raycastQuery(from: screenPos, allowing: .existingPlaneGeometry, alignment: object.allowedAlignment) else { return }
        let hitResults = sceneView.session.raycast(query)
        guard let touchOnPlane = hitResults.first(where: { touchedPlane in
            let planeNode = sceneView.node(for: touchedPlane.anchor!)
            return object.parent == planeNode
        }) else { return }
        object.simdWorldPosition = touchOnPlane.worldTransform.translation
    }
    
    private func updatedTrackingPosition(for object: VirtualObject, from gesture: UIPanGestureRecognizer) -> CGPoint {
        let translation = gesture.translation(in: sceneView)
        let currentPosition = currentTrackingPosition ?? CGPoint(sceneView.projectPoint(object.worldPosition))
        let updatedPosition = CGPoint(x: currentPosition.x + translation.x, y: currentPosition.y + translation.y)
        currentTrackingPosition = updatedPosition
        return updatedPosition
    }

    private func objectInteracting(with gesture: UIGestureRecognizer, in view: ARSCNView) -> VirtualObject? {
        for index in 0..<gesture.numberOfTouches {
            let touchLocation = gesture.location(ofTouch: index, in: view)
            
            // Look for an object directly under the `touchLocation`.
            if let object = sceneView.virtualObject(at: touchLocation) {
                return object
            }
        }
        
        // As a last resort look for an object under the center of the touches.
        if let center = gesture.center(in: view) {
            return sceneView.virtualObject(at: center)
        }
        
        return nil
    }
    
    private func closestResult(for point: CGPoint) -> ARRaycastResult? {
        guard let query = sceneView.getRaycastQuery() else { return nil }
        return sceneView.session.raycast(query).first
    }
    
    func deselectObject() {
        selectedObject?.textNode.isHidden = true
        selectedObject = nil
    }
    
    private func selectObject(_ node: VirtualObject) {
        selectedObject = node
        node.textNode.isHidden = false
    }
    
}

extension ARSCNView {
    func virtualObject(at point: CGPoint) -> VirtualObject? {
        let options: [SCNHitTestOption: Any] = [.backFaceCulling: false, .searchMode: SCNHitTestSearchMode.all.rawValue]
        let hitResults = hitTest(point, options: options)
        let touchedPictureNodes = hitResults.compactMap { $0.node as? VirtualObject }
        return touchedPictureNodes.first
    }
}

extension UIGestureRecognizer {
    func center(in view: UIView) -> CGPoint? {
        guard numberOfTouches > 0 else { return nil }

        let first = CGRect(origin: location(ofTouch: 0, in: view), size: .zero)

        let touchBounds = (1..<numberOfTouches).reduce(first) { touchBounds, index in
            return touchBounds.union(CGRect(origin: location(ofTouch: index, in: view), size: .zero))
        }

        return CGPoint(x: touchBounds.midX, y: touchBounds.midY)
    }
}

extension CGPoint {
    /// Extracts the screen space point from a vector returned by SCNView.projectPoint(_:).
    init(_ vector: SCNVector3) {
        self.init(x: CGFloat(vector.x), y: CGFloat(vector.y))
    }
}

// MARK: - Placing object
extension VirtualObjectInteraction {
    func addNewPicture(_ picture: UIImage, tappedPlaneNode: SCNNode, worldTransform: float4x4) {
        let pictureNode = VirtualObject(image: picture, heightInMeters: 0.5)
        tappedPlaneNode.addChildNode(pictureNode)
        pictureNode.simdWorldPosition = worldTransform.translation
        model.pictureToAdd = nil
        selectObject(pictureNode)
    }
}

extension ARPlaneAnchor {
    func node(in sceneView: ARSCNView) -> SCNNode? {
        sceneView.node(for: self)
    }
}

extension float4x4 {
    var translation: SIMD3<Float> {
        get {
            let translation = columns.3
            return [translation.x, translation.y, translation.z]
        }
        set(newValue) {
            columns.3 = [newValue.x, newValue.y, newValue.z, columns.3.w]
        }
    }
    
    var orientation: simd_quatf {
        return simd_quaternion(self)
    }
}
