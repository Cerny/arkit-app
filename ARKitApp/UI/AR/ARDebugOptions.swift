//
//  ARDebugOptions.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import ARKit

protocol ARDebugOptionsDelegate: class {
    func updateDebugOptions()
}

class ARDebugOptions {
    
    var showFeaturePoints: Bool {
        didSet { delegate?.updateDebugOptions() }
    }
    /// axes: +x: red, +y: green, +z: blue
    var showWorldOrigin: Bool {
        didSet { delegate?.updateDebugOptions() }
    }
    var showGrids: Bool {
        didSet { delegate?.updateDebugOptions() }
    }
    var showStatistics: Bool {
        didSet { delegate?.updateDebugOptions() }
    }
    
    weak var delegate: ARDebugOptionsDelegate?
    
    var sceneViewDebugOptions: SCNDebugOptions {
        var debugOptions = SCNDebugOptions()
        if showFeaturePoints {
            debugOptions.insert(.showFeaturePoints)
        }
        if showWorldOrigin {
            debugOptions.insert(.showWorldOrigin)
        }
        
//        debugOptions.insert(.showLightExtents)
//        debugOptions.insert(.showLightInfluences)
        return debugOptions
    }
    
    init() {
        showFeaturePoints = true
        showGrids = developmentMode
        showStatistics = false
        showWorldOrigin = false
    }
    
}
