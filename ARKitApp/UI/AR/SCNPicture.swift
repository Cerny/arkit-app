//
//  SCNPicture.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import SceneKit
import ARKit

class SCNPicture: SCNNode {
    
    //   _______________
    //  /_____________ /|
    // |              | |
    // |height        | |
    // |______________|/length
    //    width
    
    enum Constants {
        static let pictureLength: CGFloat = 0.002 // 2 mm
        static let matLength: CGFloat = 0.001 // 1 mm
    }
    
    private var pictureGeometry: SCNBox!
    private var matGeometry = SCNBox()
    var textNode: SCNNode!
    private var frameNode: SCNFrame?
    
    var allowedAlignment: ARRaycastQuery.TargetAlignment = .any
    private(set) var pictureAdjustments = PictureAdjustments()
    private var yRotation: Float = 0
    var effect: Effect? {
        didSet { updateImage() }
    }
    var colorAdjustments = PictureColor() {
        didSet { updateImage() }
    }
    let originalImage: UIImage
    private(set) var image: UIImage {
        set {
            let imageMaterial = SCNMaterial()
            imageMaterial.diffuse.contents = newValue
            pictureGeometry.materials[2] = imageMaterial
            pictureGeometry.materials[0] = imageMaterial
        }
        get {
            pictureGeometry.materials[0].diffuse.contents as! UIImage
        }
    }
    
    /// Size of the picture itself, frame and mat are not included
    var pictureSizeInMeters: CGSize {
        set {
            guard sizeIsBiggerThanMinSize(newValue) else { return }
            updateSize(newSize: newValue)
        }
        get { return CGSize(width: pictureGeometry.width, height: pictureGeometry.height) }
    }
    
    /// Size of the mat from the front view position
    private var mat2DSizeInMeters: CGSize {
        let matWidth = pictureAdjustments.pictureMat.widthInMeters
        return .init(width: pictureSizeInMeters.width + 2*matWidth, height: pictureSizeInMeters.height + 2*matWidth)
    }
    
    override var transform: SCNMatrix4 {
        get { super.transform }
        set {
            super.transform = newValue
            placeTextAbovePicture()
        }
    }
    
    init(image: UIImage, sizeInMeters: CGSize) {
        self.originalImage = image
        super.init()
        setupGeometry(sizeInMeters: sizeInMeters)
        setupNodes()
        placeTextAbovePicture()
        updateImage()
        updateFrameSize()
        updateMatSize()
        updateMatColor()
        placeTextAbovePicture()
        
        eulerAngles.x = -.pi/2
    }
    
    convenience init(image: UIImage, heightInMeters: CGFloat) {
        let aspectRatio = image.size.width / image.size.height
        let size = CGSize(width: heightInMeters * aspectRatio, height: heightInMeters)
        self.init(image: image, sizeInMeters: size)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupGeometry(sizeInMeters: CGSize) {
        let pictureGeometry = makePictureGeometry(sizeInMeters: sizeInMeters)
        self.pictureGeometry = pictureGeometry
        self.geometry = pictureGeometry
    }
    
    private func setupNodes() {
        let shadowPlaneNode = makeShadowPlaneNode()
        addChildNode(shadowPlaneNode)
        
        let directionalLightNode = makeDirectionalLightNode()
        addChildNode(directionalLightNode)
        
        let frameNode = makeFrameNode(with: .default)
        addChildNode(frameNode)
        self.frameNode = frameNode
        
        let matNode = makeMatNode()
        addChildNode(matNode)
        
        let textNode = makeTextNode()
        addChildNode(textNode)
        self.textNode = textNode
    }
    
    private func makePictureGeometry(sizeInMeters: CGSize) -> SCNBox {
        let boxGeometry = SCNBox(width: sizeInMeters.width, height: sizeInMeters.height, length: Constants.pictureLength, chamferRadius: 0)
        let whiteMaterial = SCNMaterial()
        whiteMaterial.diffuse.contents = UIColor.white
        whiteMaterial.isDoubleSided = true // TODO - when the front side will be opaque, this can be deleted
        boxGeometry.materials = Array.init(repeating: whiteMaterial, count: 6)
        return boxGeometry
    }
    
    private func makePictureNode(geometry: SCNGeometry) -> SCNNode {
        let node = SCNNode(geometry: geometry)
        return node
    }
    
    private func makeShadowPlaneNode() -> SCNNode {
        let plane = SCNPlane()
        let planeNode = SCNNode(geometry: plane)
        planeNode.castsShadow = false
        planeNode.geometry?.materials.first?.colorBufferWriteMask = []
        planeNode.position = .init(0, 0, -0.001)
        planeNode.scale = .init(5, 5, 1)
        return planeNode
    }
    
    private func makeDirectionalLightNode() -> SCNNode {
        let directionalLightNode = SCNNode()
        directionalLightNode.eulerAngles.x = -.pi/4
        directionalLightNode.eulerAngles.y = -.pi/6
        directionalLightNode.position = .init(x: 0, y: 0, z: 0.5)
        let directionalLight = SCNLight()
        directionalLight.type = .directional
        directionalLight.shadowSampleCount = 10
        directionalLight.shadowMapSize = .init(width: 1024, height: 1024)
        directionalLight.shadowColor = UIColor.black.withAlphaComponent(0.45)
        directionalLight.castsShadow = true
        directionalLight.shadowMode = .deferred
        directionalLightNode.light = directionalLight
        return directionalLightNode
    }
    
    private func makeFrameNode(with frame: RealFrame) -> SCNFrame {
        return SCNFrame(frame: frame)
    }
    
    private func makeMatNode() -> SCNNode {
        let matNode = SCNNode(geometry: matGeometry)
        return matNode
    }
    
    private func makeTextNode() ->SCNNode {
        let textNodeMaterial = SCNMaterial()
        textNodeMaterial.diffuse.contents = UIColor.black
        
        let textGeometry = SCNText(string: getText(), extrusionDepth: 1)
        textGeometry.font = UIFont.systemFont(ofSize: 10)
        textGeometry.materials = [textNodeMaterial]
        
        let textNode = SCNNode(geometry: textGeometry)
        textNode.scale = SCNVector3Make(0.005, 0.005, 0.005)

        return textNode
    }
    
    private func updateImage() {
        image = processImage()
    }
    
    func processImage() -> UIImage {
        guard let ciImage = CIImage(image: originalImage) else { return originalImage }
        
        let processedImage = ciImage
            .applying(effect: effect)
            .applying(
                brightness: colorAdjustments.ciFilterBrightness,
                contrast: colorAdjustments.ciFilterContrast,
                saturation: colorAdjustments.ciFilterSaturation
            )
        guard let resultCGImage = CIContext().createCGImage(processedImage, from: processedImage.extent) else { return originalImage }
        return UIImage(cgImage: resultCGImage)
    }
    
    private func updateText() {
        (textNode.geometry as? SCNText)?.string = getText()
    }
    
    private func getText() -> String {
        let sizeInCM = CGSize(width: pictureSizeInMeters.width * 100, height: pictureSizeInMeters.height * 100)
        return "\(Int(sizeInCM.width))cm x \(Int(sizeInCM.height))cm"
    }
    
    private func updateSize(newSize: CGSize) {
        pictureGeometry.width = newSize.width
        pictureGeometry.height = newSize.height
        updateText()
        updateFrameSize()
        updateMatSize()
        placeTextAbovePicture()
    }
    
    func sizeIsBiggerThanMinSize(_ checkedSize: CGSize) -> Bool {
        let minSize: CGFloat = 0.01 // picture has to have at least 1 cm
        return checkedSize.width > minSize && checkedSize.height > minSize
    }
    
    private func updateMatSize() {
        matGeometry.width = mat2DSizeInMeters.width
        matGeometry.height = mat2DSizeInMeters.height
        matGeometry.length = Constants.matLength
    }
    
    private func updateMatColor() {
        let matMaterial = SCNMaterial()
        matMaterial.diffuse.contents = pictureAdjustments.pictureMat.color
        matMaterial.isDoubleSided = true
        matGeometry.materials = Array.init(repeating: matMaterial, count: 6)
    }
    
    func updateFrame(newFrame: RealFrame?) {
        self.frameNode?.removeFromParentNode()
        guard let newFrame = newFrame else {
            return
        }
        self.pictureAdjustments.pictureFrame = newFrame
        let newFrameNode = makeFrameNode(with: newFrame)
        addChildNode(newFrameNode)
        self.frameNode = newFrameNode
        updateFrameSize()
        placeTextAbovePicture()
    }
    
    func updateMat(newMat: PictureMat) {
        self.pictureAdjustments.pictureMat = newMat
        updateMatSize()
        updateMatColor()
        updateFrameSize()
        placeTextAbovePicture()
    }
    
    func rotate90DegreesClockwise() {
        yRotation -= .pi/2
        eulerAngles.y = yRotation
        placeTextAbovePicture()
    }
    
    private func updateFrameSize() {
        let matSize = mat2DSizeInMeters
        frameNode?.resize(innerSize: CGSize(width: matSize.width, height: matSize.height))
    }
    
    private func placeTextAbovePicture() {
        let totalPictureHeight = Float(frameNode?.sizeInMeters.height ?? mat2DSizeInMeters.height)
        textNode.position = SCNVector3(0, totalPictureHeight / 2 + 0.05, 0)
        // center pivot
        let (min, max) = textNode.boundingBox
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = min.y + 0.5 * (max.y - min.y)
        let dz = min.z + 0.5 * (max.z - min.z)
        textNode.pivot = SCNMatrix4MakeTranslation(dx, dy, dz)
    }
    
    func resize(scale: CGFloat) {
        pictureSizeInMeters = CGSize(width: pictureSizeInMeters.width * scale, height: pictureSizeInMeters.height * scale)
    }
    
}
