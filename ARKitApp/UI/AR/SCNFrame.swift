//
//  SCNFrame.swift
//  ARKitApp
//
//  Created by Michal Černý on 14.04.2021.
//

import SceneKit
import ARKit
import SceneKit.ModelIO

class SCNFrame: SCNNode {
    
    private let leftTopCorner: Corner
    private let leftBottomCorner: Corner
    private let rightTopCorner: Corner
    private let rightBottomCorner: Corner
    private var allCorners: [Corner] {
        [leftTopCorner, leftBottomCorner, rightTopCorner, rightBottomCorner]
    }
    
    private let topSegment: Segment
    private let rightSegment: Segment
    private let bottomSegment: Segment
    private let leftSegment: Segment
    private var allSegments: [Segment] {
        [topSegment, rightSegment, bottomSegment, leftSegment]
    }
    
    var sizeInMeters: CGSize {
        let width = boundingBox.max.x - boundingBox.min.x
        let height = boundingBox.max.y - boundingBox.min.y
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    init(frame: RealFrame) {
        topSegment = Segment(side: .top, frame: frame)
        rightSegment = Segment(side: .right, frame: frame)
        bottomSegment = Segment(side: .bottom, frame: frame)
        leftSegment = Segment(side: .left, frame: frame)
        
        leftTopCorner = Corner(side: .leftTop, frame: frame)
        leftBottomCorner = Corner(side: .leftBottom, frame: frame)
        rightTopCorner = Corner(side: .rightTop, frame: frame)
        rightBottomCorner = Corner(side: .rightBottom, frame: frame)
        super.init()
        
        allSegments.forEach {
            addChildNode($0)
        }
        allCorners.forEach {
            addChildNode($0)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func resize(innerSize: CGSize) {
        let h = Float(innerSize.height/2)
        let w = Float(innerSize.width/2)
        
        topSegment.simdPosition = .init(0, h, 0)
        rightSegment.simdPosition = .init(w, 0, 0)
        bottomSegment.simdPosition = .init(0, -h, 0)
        leftSegment.simdPosition = .init(-w, 0, 0)
        
        leftTopCorner.simdPosition = .init(-w, h, 0)
        leftBottomCorner.simdPosition = .init(-w, -h, 0)
        rightTopCorner.simdPosition = .init(w, h, 0)
        rightBottomCorner.simdPosition = .init(w, -h, 0)
        
        [topSegment, bottomSegment].forEach { segment in
            segment.resize(length: innerSize.width)
        }
        [leftSegment, rightSegment].forEach { segment in
            segment.resize(length: innerSize.height)
        }
    }
    
}

extension SCNFrame {
    
    class Corner: SCNNode {
        
        enum Side {
            case leftTop
            case rightTop
            case leftBottom
            case rightBottom
        }
        
        var side: Side = .rightTop
        
        convenience init(side: Side, frame: RealFrame) {
            let mdlAsset = MDLAsset(url: frame.appearance.cornerObjectURL)
            mdlAsset.loadTextures()
            let asset = mdlAsset.object(at: 0)
            asset.setColor(frame.color, for: .baseColor)
            
            self.init(mdlObject: asset)
            
            self.side = side
            
            switch side {
            case .leftBottom:
                eulerAngles.z = .pi/2
            case .leftTop:
                break
            case .rightBottom:
                eulerAngles.z = .pi
            case .rightTop:
                eulerAngles.z = -.pi/2
            }
        }
    }
    
    class Segment: SCNNode {
        
        enum Side {
            case top
            case right
            case bottom
            case left
        }
        
        var side: Side = .bottom
        
        convenience init(side: Side, frame: RealFrame) {
            let mdlAsset = MDLAsset(url: frame.appearance.sideObjectURL)
            mdlAsset.loadTextures()
            let asset = mdlAsset.object(at: 0) // extract first object
            asset.setColor(frame.color, for: .baseColor)
            
            self.init(mdlObject: asset)
            
            self.side = side
            
            switch side {
            case .left:
                eulerAngles.z = .pi/2
            case .right:
                eulerAngles.z = -.pi/2
            case .bottom:
                eulerAngles.z = .pi
            case .top:
                break
            }
        }
        
        func resize(length: CGFloat) {
            let modelLength = boundingBox.max.x - boundingBox.min.x
            scale.x = Float(length) / modelLength
        }
    }
    
}

extension MDLObject {
    
    func setColor(_ color: UIColor, for semantic: MDLMaterialSemantic) {
        let colorProp = MDLMaterialProperty(name: "customColor", semantic: .baseColor, color: color.cgColor)
        
        let mesh = self.findMeshInChildren()
        for submesh in mesh?.submeshes ?? [] {
            let submeshMaterial = (submesh as? MDLSubmesh)?.material
            
            let baseColors = submeshMaterial?.properties(with: .baseColor)
            baseColors?.forEach { submeshMaterial?.remove($0) }
            submeshMaterial?.setProperty(colorProp)
        }
    }
    
    func findMeshInChildren() -> MDLMesh? {
        findMesh(in: self)
    }
    
    private func findMesh(in object: MDLObject) -> MDLMesh? {
        if let mesh = object as? MDLMesh {
            return mesh
        }
        for child in object.children.objects {
            if let mesh = findMesh(in: child) {
                return mesh
            }
        }
        return nil
    }
}
