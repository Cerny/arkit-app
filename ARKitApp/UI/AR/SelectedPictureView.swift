//
//  SelectedPictureView.swift
//  ARKitApp
//
//  Created by Michal Černý on 05.12.2020.
//

import UIKit

class SelectedPictureView: UIView {
    
    private weak var imageView: UIImageView!
    weak var button: UIButton!
    
    var image: UIImage? {
        set {
            imageView.image = newValue
            if let image = newValue {
                let aspectRatio = image.size.width / image.size.height
                imageView.removeConstraints([.height])
                let height = imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1/aspectRatio)
                height.priority = .defaultHigh
                height.isActive = true
            }
        }
        get {
            imageView.image
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        clipsToBounds = false
        
        let wrappingView = UIView()
        wrappingView.layer.cornerRadius = 20
        wrappingView.translatesAutoresizingMaskIntoConstraints = false
        wrappingView.addShadow()
        addSubview(wrappingView)
        wrappingView.setConstraintsEqualToSuperview()
        
        let imageView = UIImageView()
        addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        wrappingView.addSubview(imageView)
        imageView.setConstraintsEqualToSuperview()
        self.imageView = imageView
        
        let button = UIButton(type: .system)
        button.setImage(Asset.deleteIcon.image, for: .normal)
        let buttonSize: CGFloat = 50
        button.layer.cornerRadius = buttonSize/2
        button.tintColor = .red
        button.backgroundColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        self.button = button
        
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: buttonSize),
            button.widthAnchor.constraint(equalTo: button.heightAnchor),
            button.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: imageView.bottomAnchor),
            wrappingView.widthAnchor.constraint(equalToConstant: 140),
            wrappingView.heightAnchor.constraint(lessThanOrEqualToConstant: 140),
        ])
    }
    
}
