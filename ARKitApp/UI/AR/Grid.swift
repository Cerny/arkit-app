//
//  Grid.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import Foundation
import SceneKit
import ARKit

extension ARPlaneAnchor {
    var width: Float { return extent.x}
    var length: Float { return extent.z}
}

class Grid: SCNNode {
    
    var anchor: ARPlaneAnchor
    var planeGeometry: SCNPlane!
    var textGeometry: SCNText!
    let id: Int
    var volume: Float {
        anchor.width * anchor.length
    }
    
    init(anchor: ARPlaneAnchor, id: Int) {
        self.anchor = anchor
        self.id = id
        super.init()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(anchor: ARPlaneAnchor) {
        planeGeometry.width = CGFloat(anchor.extent.x);
        planeGeometry.height = CGFloat(anchor.extent.z);
        position = SCNVector3Make(anchor.center.x, 0, anchor.center.z);
        
        let planeNode = childNodes.first!
        planeNode.physicsBody = SCNPhysicsBody(type: .static, shape: SCNPhysicsShape(geometry: planeGeometry, options: nil))
        
        updateText()
    }
    
    private func setup() {
        planeGeometry = SCNPlane(width: CGFloat(anchor.width), height: CGFloat(anchor.length))
        
        let material = SCNMaterial()
        material.diffuse.contents = UIImage(named:"overlay_grid.png")
        material.lightingModel = .constant // so it doesn't receive shadows
        
        planeGeometry.materials = [material]
        let planeNode = SCNNode(geometry: planeGeometry)
        planeNode.physicsBody = SCNPhysicsBody(type: .static, shape: SCNPhysicsShape(geometry: planeGeometry, options: nil))
        planeNode.physicsBody?.categoryBitMask = 2
        
        planeNode.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z);
        planeNode.transform = SCNMatrix4MakeRotation(Float(-Double.pi / 2.0), 1.0, 0.0, 0.0);
        planeNode.castsShadow = false
        
        let textNodeMaterial = SCNMaterial()
        textNodeMaterial.diffuse.contents = UIColor.black
        
        // Set up text geometry
        textGeometry = SCNText(string: String(format: "%.1f\"", anchor.width) + " x " + String(format: "%.1f\"", anchor.length), extrusionDepth: 1)
        textGeometry.font = UIFont.systemFont(ofSize: 10)
        textGeometry.materials = [textNodeMaterial]
        
        // Integrate text node with text geometry
        let textNode = SCNNode(geometry: textGeometry)
        textNode.name = "textNode"
        textNode.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z);
        textNode.transform = SCNMatrix4MakeRotation(Float(-Double.pi / 2.0), 1.0, 0.0, 0.0);
        textNode.scale = SCNVector3Make(0.005, 0.005, 0.005)
        textNode.castsShadow = false
        
        addChildNode(textNode)
        addChildNode(planeNode)
        
        updateText()
    }
    
    private func updateText() {
        if let textGeometry = childNode(withName: "textNode", recursively: true)?.geometry as? SCNText {
            // Update text to new size
            // let volumeString = String(format: "%.2fm", volume)
            // textGeometry.string = "id: \(id), " + volumeString
            let dimensionString = String(format: "%.1fm", locale: .current, anchor.width) + " x " + String(format: "%.1fm", locale: .current, anchor.length)
            textGeometry.string = dimensionString
        }
    }
    
}
