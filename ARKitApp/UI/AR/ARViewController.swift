//
//  ARViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit
import ARKit
import SceneKit
import Combine

let developmentMode = false

class ARViewController: UIViewController {
    
    enum Constants {
        static let defaultAmbientIntensity: CGFloat = 1000
        static let defaultAmbientColorTemperature: CGFloat = 6500
    }
    
    enum BottomButtons {
        case pictureAdjustments, generalMenu
    }

    private weak var selectedPictureView: SelectedPictureView!
    @IBOutlet weak var bottomButtonsBarView: UIView!
    @IBOutlet private weak var pictureAdjustmentsView: UIView?
    @IBOutlet weak var addObjectButton: UIButton!
    
    private weak var sceneView: ARSCNView!
    private let config = ARWorldTrackingConfiguration()
    private var grids: [Grid] = []
    private lazy var pictureAdjustmentsViewController = PictureAdjustmentsViewController(hiddenOptions: [.shadow], contentView: pictureAdjustmentsView, viewController: self)
    private let menuViewController = ARMenuViewController()
    private lazy var debugOptions = ARDebugOptions()
    let model = ARModel()
    private var disposeBag = Set<AnyCancellable>()
    private lazy var virtualObjectInteraction = VirtualObjectInteraction(sceneView: sceneView, viewController: self)
    private var ambientLight: SCNLight!
    private var bottomButtons: AnyPublisher<BottomButtons, Never> {
//        return Just(BottomButtons.pictureAdjustments).eraseToAnyPublisher() // uncomment for debug
        return virtualObjectInteraction.$selectedObject.map { $0 == nil ? .generalMenu : .pictureAdjustments }.eraseToAnyPublisher()
    }
    let coachingOverlay = ARCoachingOverlayView()
    var focusSquare = FocusSquare()
    var session: ARSession {
        sceneView.session
    }
    let updateQueue = DispatchQueue(label: "com.MichalCerny.ARKitApp.serialSceneKitQueue")
    var lastRecognizedPlaneId = 0
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSceneView()
        setupSceneView()
        sceneView.scene.rootNode.addChildNode(focusSquare)
        setupUI()
        pictureAdjustmentsViewController.delegate = self
        updateDebugOptions()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupBindings()
        selectedPictureView.button.addTarget(self, action: #selector(deletePictureToAdd), for: .touchUpInside)
        addCoachingView()
        virtualObjectInteraction.setupGestureRecognizers()
        addAmbientLight()
        
        // Uncomment for debugging:
        // let pictureNode = VirtualObject(image: Asset.uhk.image, heightInMeters: 0.5)
        // sceneView.scene.rootNode.addChildNode(pictureNode)
    }
    
    private func setupBindings() {
        bottomButtons
            .removeDuplicates()
            .sink { [weak self] option in
                self?.updateBottomButtons(to: option)
            }
            .store(in: &disposeBag)
        
        model.$pictureToAdd
            .sink { [weak self] picture in
                guard let self = self else { return }
                self.view.bringSubviewToFront(self.selectedPictureView)
                self.selectedPictureView.image = picture
                self.selectedPictureView.isHidden = picture == nil
                self.addObjectButton.isHidden = picture == nil
            }
            .store(in: &disposeBag)
        
        virtualObjectInteraction.$selectedObject
            .compactMap { $0 }
            .sink { [weak self] picture in
                self?.pictureAdjustmentsViewController.setup(with: picture)
                self?.pictureAdjustmentsViewController.updateContentView()
            }
            .store(in: &disposeBag)
        
        virtualObjectInteraction.$selectedObject
            .sink { [weak self] picture in
                self?.addObjectButton.isHidden = picture != nil
            }
            .store(in: &disposeBag)
        
        menuViewController.pictureToAdd
            .sink { [weak self] in
                self?.model.pictureToAdd = $0
            }
            .store(in: &disposeBag)
        
        menuViewController.shouldReset
            .sink { [weak self] in
                self?.reset()
            }
            .store(in: &disposeBag)
        
        menuViewController.areGridsVisible
            .sink { [weak self] in
                self?.debugOptions.showGrids = $0
            }
            .store(in: &disposeBag)
    }
    
    private func addAmbientLight() {
        let ambientLightNode = SCNNode()
        let ambientLight = SCNLight()
        ambientLight.intensity = Constants.defaultAmbientIntensity
        ambientLight.type = .ambient
        ambientLightNode.light = ambientLight
        sceneView.scene.rootNode.addChildNode(ambientLightNode)
        self.ambientLight = ambientLight
    }
    
    private func addCoachingView() {
        guard !developmentMode else { return }
        view.addSubview(coachingOverlay)
        coachingOverlay.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            coachingOverlay.topAnchor.constraint(equalTo: view.topAnchor),
            coachingOverlay.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            coachingOverlay.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            coachingOverlay.bottomAnchor.constraint(equalTo: bottomButtonsBarView.topAnchor)
        ])
        view.bringSubviewToFront(coachingOverlay)
        
        coachingOverlay.goal = .verticalPlane
        coachingOverlay.session = sceneView.session
    }
    
    private func updateBottomButtons(to option: BottomButtons) {
        // Remove old controller
        [pictureAdjustmentsViewController, menuViewController].forEach {
            $0.view.removeFromSuperview()
            $0.removeFromParent()
            $0.didMove(toParent: nil)
        }
        // Set new controller
        var controller: UIViewController {
            switch  option {
            case .generalMenu: return menuViewController
            case .pictureAdjustments: return pictureAdjustmentsViewController
            }
        }
        set(controller, toView: bottomButtonsBarView)
        
        // Setup controller
        switch option {
        case .generalMenu:
            pictureAdjustmentsView?.isHidden = true
            menuViewController.gridsSwitch.isOn = debugOptions.showGrids
        case .pictureAdjustments:
            pictureAdjustmentsViewController.updateContentView()
        }
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            view.frame.origin.y = -keyboardSize.height
        }
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        view.frame.origin.y = 0
    }
    
    private func addSceneView() {
        let sceneView = ARSCNView()
        view.addSubview(sceneView)
        sceneView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sceneView.topAnchor.constraint(equalTo: view.topAnchor),
            sceneView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            sceneView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            sceneView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        view.sendSubviewToBack(sceneView)
        self.sceneView = sceneView
    }
    
    private func setupSceneView() {
        config.planeDetection = developmentMode ? [.vertical, .horizontal] : [.vertical]
        // Shadows doesn't work when enabled
//        if ARWorldTrackingConfiguration.supportsFrameSemantics(.personSegmentationWithDepth)  {
//            config.frameSemantics.insert(.personSegmentationWithDepth)
//        }
        config.isLightEstimationEnabled = true
        sceneView.session.run(config)
        sceneView.delegate = self
        sceneView.session.delegate = self
        debugOptions.delegate = self
    }
    
    @objc private func deletePictureToAdd() {
        model.pictureToAdd = nil
    }
    
    private func reset() {
        sceneView.session.run(config, options: .resetTracking)
        removeGrids()
        Logger.normal("Reset tracking", category: .detectedPlanes)
    }
    
    @IBAction func addObjectButtonClicked(_ sender: Any) {
        if let pictureToAdd = model.pictureToAdd,
           let query = sceneView.getRaycastQuery(),
           let raycastResultWithPlane = sceneView.session.raycast(query).first,
           let tappedPlaneNode = (raycastResultWithPlane.anchor as? ARPlaneAnchor)?.node(in: sceneView) {
            let notTransparentImage = pictureToAdd.changeTransparentPixels(to: .white)
            virtualObjectInteraction.addNewPicture(notTransparentImage, tappedPlaneNode: tappedPlaneNode, worldTransform: raycastResultWithPlane.worldTransform)
        }
    }
       
}

extension ARViewController: ARSessionDelegate {
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        let lightEstimate = frame.lightEstimate
        ambientLight.intensity = lightEstimate?.ambientIntensity ?? Constants.defaultAmbientIntensity
        ambientLight.temperature = lightEstimate?.ambientColorTemperature ?? Constants.defaultAmbientColorTemperature
    }
}

extension ARViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            self.updateFocusSquare()
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        for grid in grids where grid.anchor == anchor {
            logGridOperation(.remove, grid: grid)
        }
        grids.removeAll(where: { $0.anchor == anchor } )
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        addGrid(to: node, anchor: anchor)
        if anchor is ARPlaneAnchor, !developmentMode {
            debugOptions.showFeaturePoints = false
        }
    }
    
    private func addGrid(to node: SCNNode, anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        lastRecognizedPlaneId += 1
        let grid = Grid(anchor: planeAnchor, id: lastRecognizedPlaneId)
        grid.opacity = debugOptions.showGrids ? 1 : 0
        grids.append(grid)
        node.addChildNode(grid)
        logGridOperation(.add, grid: grid)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        updateGrid(anchor: anchor)
    }
    
    private func updateGrid(anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        guard let foundGrid = grids.first(where: { $0.anchor.identifier == planeAnchor.identifier}) else {
            return
        }
        foundGrid.update(anchor: planeAnchor)
        logGridOperation(.update, grid: foundGrid)
    }
    
    private func removeGrids() {
        grids.removeAll()
    }
    
    enum GridOperation: String {
        case add, remove, update
    }
    
    private func logGridOperation(_ operation: GridOperation, grid: Grid) {
        // Uncomment for debugging:
        // let idString = String(format: "%03d", grid.id)
        // Logger.normal("id: \(idString), \(operation.rawValue), volume: \(grid.volume)", category: .detectedPlanes)
    }
    
}

extension ARViewController: PictureAdjustmentsViewControllerDelegate {
    func didChangeColor(_ newColor: PictureColor) {
        virtualObjectInteraction.selectedObject?.colorAdjustments = newColor
    }
    
    func matViewController(_ matVC: MatViewController, didUpdateMat newMat: PictureMat) {
        virtualObjectInteraction.selectedObject?.updateMat(newMat: newMat)
    }
    
    func didSelectFrame(frame: RealFrame?) {
        virtualObjectInteraction.selectedObject?.updateFrame(newFrame: frame)
    }
    
    func pictureAdjustmentsViewControllerRequestedDelete() {
        guard let selectedPictureNode = virtualObjectInteraction.selectedObject else { return }
        selectedPictureNode.removeFromParentNode()
        virtualObjectInteraction.deselectObject()
    }
    
    func didRequestRotate() {
        guard let selectedObject = virtualObjectInteraction.selectedObject else { return }
        selectedObject.rotate90DegreesClockwise()
    }
    
    func didSelectEffect(_ effect: Effect?) {
        virtualObjectInteraction.selectedObject?.effect = effect
    }
    
    func updateFocusSquare() {
        let isAddingAvailable = virtualObjectInteraction.selectedObject == nil && model.pictureToAdd != nil
        if coachingOverlay.isActive || !isAddingAvailable {
            focusSquare.hide()
        } else {
            focusSquare.unhide()
        }
        
        // Perform ray casting only when ARKit tracking is in a good state.
        if let camera = session.currentFrame?.camera, case .normal = camera.trackingState,
            let query = sceneView.getRaycastQuery(),
            let result = sceneView.castRay(for: query).first,
            isAddingAvailable {
            
            updateQueue.async {
                self.sceneView.scene.rootNode.addChildNode(self.focusSquare)
                self.focusSquare.state = .detecting(raycastResult: result, camera: camera)
            }
            if !coachingOverlay.isActive && model.pictureToAdd != nil {
                addObjectButton.isHidden = false
            }
        } else {
            updateQueue.async {
                self.focusSquare.state = .initializing
                self.sceneView.pointOfView?.addChildNode(self.focusSquare)
            }
            addObjectButton.isHidden = true
        }
    }
    
}

extension ARViewController: ARDebugOptionsDelegate {
    
    func updateDebugOptions() {
        sceneView.debugOptions = debugOptions.sceneViewDebugOptions
        grids.forEach {
            $0.opacity = debugOptions.showGrids ? 1 : 0
        }
        sceneView.showsStatistics = debugOptions.showStatistics
    }
    
}

// MARK: - UI
extension ARViewController {
    private func setupUI() {
        addSelectedImageView()
        addObjectButton.addShadow()
        addObjectButton.layer.cornerRadius = addObjectButton.frame.height / 2
    }
    
    private func addSelectedImageView() {
        let selectedPicture = SelectedPictureView()
        view.addSubview(selectedPicture)
        NSLayoutConstraint.useAndActivate([
            selectedPicture.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24),
            selectedPicture.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 24),
        ])
        self.selectedPictureView = selectedPicture
    }
}

extension ARSCNView {
    // - Tag: CastRayForFocusSquarePosition
    func castRay(for query: ARRaycastQuery) -> [ARRaycastResult] {
        return session.raycast(query)
    }
    
    // - Tag: GetRaycastQuery
    func getRaycastQuery(for alignment: ARRaycastQuery.TargetAlignment = .any) -> ARRaycastQuery? {
        return raycastQuery(from: screenCenter, allowing: .existingPlaneGeometry, alignment: .any)
    }
    
    var screenCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
}
