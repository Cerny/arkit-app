//
//  ImagePickerViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 05.12.2020.
//

import UIKit
import Combine

class ImagePickerViewController: UIViewController {
    
    var selectedImage = PassthroughSubject<UIImage, Never>()
    private var imagePicker = UIImagePickerController()
    
    init(source: UIImagePickerController.SourceType) {
        super.init(nibName: nil, bundle: nil)
        imagePicker.delegate = self
        imagePicker.sourceType = source
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        set(imagePicker)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ImagePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true)
        guard let selectedImage = info[.originalImage] as? UIImage else { return }
        self.selectedImage.send(selectedImage)
    }
}
