//
//  FrameColorPickerViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 16.04.2021.
//

import UIKit
import Colorful

protocol FrameColorPickerViewControllerDelegate: class {
    func didSelectColor(_ color: UIColor, with frameAppearance: RealFrame.Appearance)
}

class FrameColorPickerViewController: UIViewController {

    @IBOutlet weak var colorPicker: ColorPicker!
    @IBOutlet weak var okButton: UIButton!
    
    weak var delegate: FrameColorPickerViewControllerDelegate?
    let frame: RealFrame.Appearance
    let initColor: UIColor
    
    init(frame: RealFrame.Appearance, initColor: UIColor) {
        self.frame = frame
        self.initColor = initColor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        okButton.setTitle(L10n.ok, for: .normal)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        colorPicker.set(color: initColor, colorSpace: .extendedSRGB)
        okButton.backgroundColor = .white
        okButton.layer.cornerRadius = 16
        okButton.addShadow()
    }
    
    @IBAction func okButtonClicked(_ sender: Any) {
        dismiss(animated: true)
        delegate?.didSelectColor(colorPicker.color, with: frame)
    }
    
}
