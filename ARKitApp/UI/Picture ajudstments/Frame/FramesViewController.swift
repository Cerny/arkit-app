//
//  FramesViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 16.04.2021.
//

import UIKit


protocol FramesViewControllerDelegate: class {
    func didSelectFrame(frame: RealFrame?)
}

class FramesViewController: UIViewController {
    
    struct FrameOption: Hashable {
        let image: UIImage
        let frame: RealFrame.Appearance?
    }
    
    typealias CollectionViewModel = FrameOption
    
    weak var delegate: FramesViewControllerDelegate?
    private weak var collectionView: UICollectionView!
    private lazy var dataSource = makeDataSource()
    private let frames: [CollectionViewModel] = {
        // TODO: add none frame option
        let options: [FrameOption] = RealFrame.Appearance.allCases.compactMap { option in
            option.image.map { FrameOption(image: $0, frame: option) }
        }
        return options
    }()
    private var image: UIImage?
    private var currentFrame: RealFrame?
    
    override func loadView() {
        super.loadView()
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCellsWithClasses([FrameCollectionViewCell.self])
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        setupFrames()
    }
    
    func setup(frame: RealFrame?) {
        self.currentFrame = frame
        if isViewLoaded {
            setupFrames()
        }
    }
    
    private func makeDataSource() -> UICollectionViewDiffableDataSource<Int, CollectionViewModel> {
        UICollectionViewDiffableDataSource<Int, CollectionViewModel>(
            collectionView: collectionView,
            cellProvider: { collectionView, indexPath, frame -> UICollectionViewCell? in
                let cell = collectionView.dequeueReusableCell(ofClass: FrameCollectionViewCell.self, for: indexPath)
                cell.setup(image: frame.image, frameOption: frame)
                return cell
            }
        )
    }
    
    private func setupFrames() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, CollectionViewModel>()
        snapshot.appendSections([0])
        snapshot.appendItems(frames, toSection: 0)
        dataSource.apply(snapshot, animatingDifferences: false)
    }
    
}

extension FramesViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let frame = frames[indexPath.row].frame {
            let colorPickerVC = FrameColorPickerViewController(frame: frame, initColor: currentFrame?.color ?? RealFrame.Constants.defaultColor)
            colorPickerVC.delegate = self
            colorPickerVC.modalPresentationStyle = .overFullScreen
            colorPickerVC.modalTransitionStyle = .crossDissolve
            present(colorPickerVC, animated: true)
        } else {
            currentFrame = nil
            delegate?.didSelectFrame(frame: nil)
        }
    }
    
}

extension FramesViewController: FrameColorPickerViewControllerDelegate {
    func didSelectColor(_ color: UIColor, with frameAppearance: RealFrame.Appearance) {
        let frame = RealFrame(appearance: frameAppearance, color: color)
        currentFrame = frame
        delegate?.didSelectFrame(frame: frame)
    }
}

// MARK: - UI
extension FramesViewController {
    
    private func setupUI() {
        addCollectionView()
    }
    
    private func addCollectionView() {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: makeCVLayout())
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8),
            collectionView.heightAnchor.constraint(equalToConstant: FrameCollectionViewCell.Constants.defaultHeight),
        ])
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        // Add tap gesture recognizer so the gestures doesn't get through
        let tapGestureRecognizer = UITapGestureRecognizer(target: nil, action: nil)
        tapGestureRecognizer.cancelsTouchesInView = false
        collectionView.addGestureRecognizer(tapGestureRecognizer)
        self.collectionView = collectionView
    }
    
    private func makeCVLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .estimated(300), heightDimension: .absolute(FrameCollectionViewCell.Constants.defaultHeight))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: .fixed(8), top: nil, trailing: nil, bottom: nil)
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: itemSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 16)
        section.orthogonalScrollingBehavior = .continuous
        section.supplementariesFollowContentInsets = false
        return UICollectionViewCompositionalLayout(section: section)
    }
    
}
