//
//  MatViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit
protocol MatViewControllerDelegate: class {
    func matViewController(_ matVC: MatViewController, didUpdateMat newMat: PictureMat)
}

class MatViewController: UIViewController {
    
    struct Constants {
        static let maxFrameAndMatSize: Float = 20
    }
    
    @IBOutlet private weak var matSizeSlider: UISlider!
    @IBOutlet private weak var matColorButton: UIButton!
    @IBOutlet private weak var matSizeLabel: UILabel!
    
    weak var delegate: MatViewControllerDelegate?
    private var mat = PictureMat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        matSizeSlider.maximumValue = Constants.maxFrameAndMatSize
        setupAppearance()
        updateValues()
    }
    
    private func setupAppearance() {
        matColorButton.layer.borderColor = UIColor(white: 0.5, alpha: 1).cgColor
        matColorButton.layer.borderWidth = 1
        matColorButton.layer.cornerRadius = matColorButton.frame.height/2
    }
    
    func setup(with pictureAdjustments: PictureAdjustments) {
        self.mat = pictureAdjustments.pictureMat
        if isViewLoaded {
            updateValues()
        }
    }
    
    private func updateValues() {
        matSizeSlider.value = Float(mat.widthInMeters) * 100
        matColorButton.backgroundColor = mat.color
        matSizeLabel.text = valueWithCM(value: Float(mat.widthInMeters) * 100)
    }
    
    private func valueWithCM(value: Float) -> String {
        String(format: "%.1f CM", locale: .current, value)
    }
    
    @IBAction func matSizeSliderChanged(_ sender: UISlider) {
        let widthInCM = sender.value
        mat.widthInMeters = CGFloat(widthInCM) / 100
        delegate?.matViewController(self, didUpdateMat: mat)
        matSizeLabel.text = valueWithCM(value: sender.value)
    }
    
    @IBAction func matColorButtonClicked(_ sender: UIButton) {
        presentColorPicker()
    }
    
    func presentColorPicker() {
        let colorPickerVC = ColorPickerViewController(initialColor: mat.color)
        colorPickerVC.modalPresentationStyle = .overFullScreen
        colorPickerVC.modalTransitionStyle = .crossDissolve
        colorPickerVC.delegate = self
        present(colorPickerVC, animated: true)
    }
    
}

extension MatViewController: ColorPickerViewControllerDelegate {
    func colorChanged(newColor: UIColor) {
        mat.color = newColor
        matColorButton.backgroundColor = newColor
        delegate?.matViewController(self, didUpdateMat: mat)
    }
}
