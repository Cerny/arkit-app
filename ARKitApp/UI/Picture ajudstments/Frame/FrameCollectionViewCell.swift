//
//  FrameCollectionViewCell.swift
//  ARKitApp
//
//  Created by Michal Černý on 16.04.2021.
//

import UIKit

class FrameCollectionViewCell: UICollectionViewCell {
 
    enum Constants {
        static let defaultHeight: CGFloat = 100
    }
    
    private weak var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 8
        self.imageView = imageView
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.setConstraintsEqualToSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(image: UIImage, frameOption: FramesViewController.FrameOption) {
        imageView.image = image
        let aspectRatio: CGFloat = image.size.width / image.size.height
        imageView.removeConstraints([.width, .height])
        let height = imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: aspectRatio)
        height.priority = .required
        height.isActive = true
    }
    
}
