//
//  CustomSlider.swift
//  ARKitApp
//
//  Created by Michal Černý on 18.04.2021.
//

import UIKit

class CustomSlider: UISlider {
    
    private weak var titleLabel: UILabel!
    private weak var valueLabel: UILabel!
    
    var title: String? {
        set { titleLabel.text = newValue }
        get { titleLabel.text }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        let titleLabel = UILabel()
        titleLabel.textColor = .white
        self.titleLabel = titleLabel
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
        ])
        
        let valueLabel = UILabel()
        valueLabel.textColor = .white
        self.valueLabel = valueLabel
        addSubview(valueLabel)
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            valueLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: super.intrinsicContentSize.width, height: super.intrinsicContentSize.height + 40)
    }
    
    func setDescriptionValue(with value: Float) {
        setDescriptionValue(with: Double(value))
    }
    
    func setDescriptionValue(with value: Double) {
        valueLabel.text = String(format: "%.2f", locale: .current, value)
    }
    
    func setDescriptionValue(with value: Int) {
        valueLabel.text = String(value)
    }
    
}
