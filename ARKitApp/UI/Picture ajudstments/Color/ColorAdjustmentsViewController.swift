//
//  ColorAdjustmentsViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 18.04.2021.
//

import UIKit

protocol AdjustColorViewControllerDelegate: class {
    func didChangeColor(_ newColor: PictureColor)
}

class ColorAdjustmentsViewController: UIViewController {

    @IBOutlet weak var brightnessSlider: CustomSlider!
    @IBOutlet weak var contrastSlider: CustomSlider!
    @IBOutlet weak var saturationSlider: CustomSlider!
    @IBOutlet weak var resetButton: UIButton!
    
    weak var delegate: AdjustColorViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSliders()
        resetButton.setTitle(L10n.reset, for: .normal)
    }
    
    private func setupSliders() {
        brightnessSlider.minimumValue = -100
        brightnessSlider.maximumValue = 100
        brightnessSlider.title = L10n.brightness
        
        contrastSlider.minimumValue = -100
        contrastSlider.maximumValue = 100
        contrastSlider.title = L10n.contrast
        
        saturationSlider.minimumValue = -100
        saturationSlider.maximumValue = 100
        saturationSlider.title = L10n.saturation
    }
    
    func setup(with color: PictureColor) {
        loadViewIfNeeded()
        brightnessSlider.value = Float(color.brightness)
        contrastSlider.value = Float(color.contrast)
        saturationSlider.value = Float(color.saturation)
        updateValueStrings(with: color)
    }

    @IBAction func sliderValueChanged(_ sender: Any) {
        let newColor = PictureColor(
            brightness: Int(brightnessSlider.value),
            contrasts: Int(contrastSlider.value),
            saturation: Int(saturationSlider.value)
        )
        updateValueStrings(with: newColor)
        delegate?.didChangeColor(newColor)
    }
    
    private func updateValueStrings(with color: PictureColor) {
        brightnessSlider.setDescriptionValue(with: Int(brightnessSlider.value))
        contrastSlider.setDescriptionValue(with: Int(contrastSlider.value))
        saturationSlider.setDescriptionValue(with: Int(saturationSlider.value))
    }

    @IBAction func resetButtonClicked(_ sender: Any) {
        let newColor = PictureColor()
        setup(with: newColor)
        delegate?.didChangeColor(newColor)
    }
    
}
