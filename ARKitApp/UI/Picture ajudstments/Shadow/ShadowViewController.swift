//
//  ShadowViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit

protocol ShadowViewControllerDelegate: class {
    func shadowOnOffButtonClicked()
    func shadowDirectionButtonClicked()
    func shadowIntensitySliderValueChanged(sender: UISlider)
    func shadowRadiusSliderValueChanged(sender: UISlider)
    func shadowButtonPan(gestureRecognizer: UIPanGestureRecognizer)
}

class ShadowViewController: UIViewController, UIGestureRecognizerDelegate {
  
}
