//
//  PictureAdjustmentsViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit

protocol PictureAdjustmentsViewControllerDelegate:
    MatViewControllerDelegate,
    EffectsViewControllerDelegate,
    FramesViewControllerDelegate,
    AdjustColorViewControllerDelegate
{
    func pictureAdjustmentsViewControllerRequestedDelete()
    func didRequestRotate()
}

class PictureAdjustmentsViewController: UIViewController {
    
    enum Option: CaseIterable {
        case delete
        case rotate
        case effects
        case mat
        case shadow
        case realFrame
        case color
    }
    
    private var buttonsByOptions: [Option: UIButton] = [:]
    weak var delegate: PictureAdjustmentsViewControllerDelegate? {
        didSet {
            matVC.delegate = delegate
            effectVC.delegate = delegate
            realFramesVC.delegate = delegate
            colorVC.delegate = delegate
        }
    }
    private var selectedOption: Option? {
        didSet {
            let shouldDeselectOption = oldValue == selectedOption && oldValue != nil
            if shouldDeselectOption {
                selectedOption = nil
            }
            updateButtonsSelection()
            updateContentView()
        }
    }
    private weak var activeAdjustmentsViewController: UIViewController?
    private weak var contentView: UIView?
    private weak var viewController: UIViewController?
    private let hiddenOptions: [Option]
    
    private let effectVC = EffectsViewController()
    private let matVC = MatViewController()
    private let realFramesVC = FramesViewController()
    private let colorVC = ColorAdjustmentsViewController()
    private var viewControllersByOptions: [Option: UIViewController] {
        [
            .effects: effectVC,
            .mat: matVC,
            .realFrame: realFramesVC,
            .color: colorVC,
        ]
    }
    
    init(hiddenOptions: [Option], contentView: UIView?, viewController: UIViewController) {
        self.contentView = contentView
        self.viewController = viewController
        self.hiddenOptions = hiddenOptions
        super.init(nibName: nil, bundle: nil)
        matVC.delegate = delegate
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(buttonsByOptions.count == Option.allCases.count, "Missing items in buttonsByOptions dictionary")
        hiddenOptions.forEach {
            button(for: $0).isHidden = true
        }
        updateButtonsSelection()
        updateContentView()
    }
    
    private func button(for option: Option) -> UIButton {
        buttonsByOptions[option]!
    }
    
    private func option(for button: UIButton) -> Option {
        buttonsByOptions.first(where: { $0.value == button })!.key
    }
    
    func resetState() {
        selectedOption = nil
    }
    
    func setup(with virtualObject: VirtualObject) {
        effectVC.setup(with: virtualObject.originalImage)
        matVC.setup(with: virtualObject.pictureAdjustments)
        colorVC.setup(with: virtualObject.colorAdjustments)
        realFramesVC.setup(frame: virtualObject.pictureAdjustments.pictureFrame)
    }
    
    private func updateButtonsSelection() {
        buttonsByOptions.forEach {
            $0.value.isSelected = false
        }
        if let selectedOption = selectedOption {
            button(for: selectedOption).isSelected = true
        }
    }
    
    func updateContentView() {
        guard let contentView = contentView else { return }
        guard let selectedOption = selectedOption,
              let contentViewController = viewControllersByOptions[selectedOption] else {
            contentView.isHidden = true
            return
        }
        contentView.isHidden = false
        activeAdjustmentsViewController?.view.removeFromSuperview()
        activeAdjustmentsViewController?.removeFromParent()
        viewController?.set(contentViewController, toView: contentView)
        activeAdjustmentsViewController = contentViewController
    }
    
    @objc private func optionButtonClicked(_ sender: UIButton) {
        let clickedOption = option(for: sender)
        switch clickedOption {
        case .effects, .mat, .realFrame, .color:
            selectedOption = clickedOption
        case .delete:
            showDeleteAlert()
            selectedOption = nil
        case .rotate:
            delegate?.didRequestRotate()
            selectedOption = nil
        case .shadow:
            return
        }
    }
    
    private func showDeleteAlert() {
        let alert = UIAlertController(title: L10n.delete, message: L10n.areYouSureDelete, preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: L10n.delete, style: UIAlertAction.Style.destructive) { [weak self] _ in
            self?.delegate?.pictureAdjustmentsViewControllerRequestedDelete()
        }
        let cancelAction = UIAlertAction(title: L10n.cancel, style: .cancel, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
}

extension PictureAdjustmentsViewController.Option {
    fileprivate var buttonTitle: String {
        switch self {
        case .delete: return L10n.delete
        case .rotate: return L10n.rotate
        case .effects: return L10n.effects
        case .mat: return L10n.mat
        case .shadow: return L10n.shadow
        case .realFrame: return L10n.realFrame
        case .color: return L10n.color
        }
    }
}

// MARK: - UI
extension PictureAdjustmentsViewController {
    
    private func setupUI() {
        var buttonsByOptions: [Option: UIButton] = [:]
        Option.allCases.forEach {
            buttonsByOptions[$0] = makeButton(title: $0.buttonTitle)
        }
        self.buttonsByOptions = buttonsByOptions
        
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.setConstraintsEqualToSuperview()
        
        let wrappingView = UIView()
        scrollView.addSubview(wrappingView)
        wrappingView.translatesAutoresizingMaskIntoConstraints = false
        wrappingView.setConstraintsEqualToSuperview()
        
        let arrangedSubviews: [UIView] = [
            button(for: .delete),
            button(for: .rotate),
            button(for: .effects),
            button(for: .realFrame),
            button(for: .mat),
            button(for: .color),
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.spacing = 16
        wrappingView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(stackView)
        
        NSLayoutConstraint.useAndActivate([
            stackView.leadingAnchor.constraint(equalTo: wrappingView.leadingAnchor, constant: 16),
            stackView.topAnchor.constraint(equalTo: wrappingView.topAnchor, constant: 4),
            stackView.bottomAnchor.constraint(equalTo: wrappingView.bottomAnchor, constant: -4),
            stackView.trailingAnchor.constraint(equalTo: wrappingView.trailingAnchor, constant: -16),
            wrappingView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        ])
    }
    
    private func makeButton(title: String) -> UIButton {
        let button = UIButton(type: .system)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: #selector(optionButtonClicked), for: .touchUpInside)
        return button
    }
    
}
