//
//  EffectsViewController.swift
//  ARKitApp
//
//  Created by Michal Černý on 02.12.2020.
//

import UIKit

protocol EffectsViewControllerDelegate: class {
    func didSelectEffect(_ effect: Effect?)
}

class EffectsViewController: UIViewController {
    
    struct EffectOption: Hashable {
        let name: String
        let effect: Effect?
    }
    
    typealias CollectionViewModel = EffectOption
    
    weak var delegate: EffectsViewControllerDelegate?
    private weak var collectionView: UICollectionView!
    private lazy var dataSource = makeDataSource()
    private let effects: [EffectOption] = {
        let defaultOption = EffectOption(name: "Default", effect: nil)
        let transformedEffects = Effect.allCases.map {
            EffectOption(name: $0.name, effect: $0)
        }
        return [defaultOption] + transformedEffects
    }()
    private var image: UIImage?
    
    override func loadView() {
        super.loadView()
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCellsWithClasses([EffectCollectionViewCell.self])
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        setupEffects()
    }
    
    func setup(with image: UIImage) {
        self.image = image
        if isViewLoaded {
            setupEffects()
        }
    }
    
    private func makeDataSource() -> UICollectionViewDiffableDataSource<Int, CollectionViewModel> {
        UICollectionViewDiffableDataSource<Int, CollectionViewModel>(
            collectionView: collectionView,
            cellProvider: { [weak self] collectionView, indexPath, effectOption -> UICollectionViewCell? in
                let cell = collectionView.dequeueReusableCell(ofClass: EffectCollectionViewCell.self, for: indexPath)
                cell.setup(originalImage: self?.image, effectOption: effectOption)
                return cell
            }
        )
    }
    
    private func setupEffects() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, CollectionViewModel>()
        snapshot.appendSections([0])
        snapshot.appendItems(effects, toSection: 0)
        dataSource.apply(snapshot, animatingDifferences: false)
    }
    
}

extension EffectsViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectEffect(effects[indexPath.row].effect)
    }
    
}

// MARK: - UI
extension EffectsViewController {
    
    private func setupUI() {
        addCollectionView()
    }
    
    private func addCollectionView() {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: makeCVLayout())
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8),
            collectionView.heightAnchor.constraint(equalToConstant: EffectCollectionViewCell.Constants.defaultHeight),
        ])
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        // Add tap gesture recognizer so the gestures doesn't get through
        let tapGestureRecognizer = UITapGestureRecognizer(target: nil, action: nil)
        tapGestureRecognizer.cancelsTouchesInView = false
        collectionView.addGestureRecognizer(tapGestureRecognizer)
        self.collectionView = collectionView
    }
    
    private func makeCVLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .estimated(300), heightDimension: .absolute(EffectCollectionViewCell.Constants.defaultHeight))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: .fixed(8), top: nil, trailing: nil, bottom: nil)
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: itemSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 16)
        section.orthogonalScrollingBehavior = .continuous
        section.supplementariesFollowContentInsets = false
        return UICollectionViewCompositionalLayout(section: section)
    }
    
}
