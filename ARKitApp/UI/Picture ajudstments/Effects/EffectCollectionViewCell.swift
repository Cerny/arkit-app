//
//  EffectCollectionViewCell.swift
//  ARKitApp
//
//  Created by Michal Černý on 04.12.2020.
//

import UIKit

class EffectCollectionViewCell: UICollectionViewCell {
 
    enum Constants {
        static let defaultHeight: CGFloat = 100
    }
    
    private weak var imageView: UIImageView!
    private weak var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 8
        self.imageView = imageView
        
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .gray
        label.font = .systemFont(ofSize: 15)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        self.label = label
        
        let stackView = UIStackView(arrangedSubviews: [imageView, label])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 1
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.setConstraintsEqualToSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(originalImage: UIImage?, effectOption: EffectsViewController.EffectOption) {
        if let effect = effectOption.effect {
            let imageWithFilter = originalImage?.process(with: effect)
            imageView.image = imageWithFilter
        } else {
            imageView.image = originalImage
        }
        label.text = effectOption.name
        
        let aspectRatio: CGFloat = {
            if let originalImage = originalImage {
                return originalImage.size.width / originalImage.size.height
            } else {
                return 1
            }
        }()
        imageView.removeConstraints([.width])
        let height = imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: aspectRatio)
        height.priority = .defaultHigh
        height.isActive = true
    }
    
}
